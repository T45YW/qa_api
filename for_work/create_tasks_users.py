import json
import requests

from lib.base_authorization import url_base, access_token_user

# Входные данные
qt_tasks = 2  # количество задач
name_task = "Task_"  # формируется name_task+user_id+номер задачи
priority = 75  # 0≤x≤50:Низкий, 51≤х≤75:Средний, 76≤х≤100:Высокий
date_done = 1720006611000000000  # срок выполнения, www.epochconverter.com. Поле обязательно, если есть отвественный + девять нулей

d_users_tasks = []  # массив, словарь задач {name: name_task}


def create_tasks():
    """Создание задач с ответственным пользователем"""
    global users_id_login
    global d_users_tasks

    def create_task(name, user_id_response):
        """Создание задачи"""

        url = url_base + "/api/task"
        payload = json.dumps({
            "name": name,
            "priority": priority,
            "userIdResponse": user_id_response,
            "dateDone": date_done
        })
        headers = {
            'Authorization': access_token_user,
            'Content-Type': 'application/json'
        }

        response = requests.request("POST", url, headers=headers, data=payload)
        task_name = response.json()["name"]
        return task_name

    for i in range(len(users_id_login)):
        d = {}
        for j in range(1, qt_tasks + 1):
            name = name_task + users_id_login[i]["login"] + "_" + str(j)
            user_id_response = users_id_login[i]["id"]
            d[users_id_login[i]["login"]] = create_task(name, user_id_response)
            d_users_tasks.append(d)

    return d_users_tasks


create_tasks()
print(d_users_tasks)

import requests
from lib.base_authorization import url_base, access_token_user


users_id_login = []

def get_users_tasks():
    """Создание списка пользователей: id и login"""
    users = []
    global users_id_login

    def get_users():
        """Получение пользователей проекта"""
        nonlocal users
        url = url_base + "/api/users"

        payload = {}
        headers = {
            'Authorization': access_token_user
        }

        response = requests.request("GET", url, headers=headers, data=payload)
        parsed_response_text = response.json()
        users = parsed_response_text[2:]
        return users

    get_users()

    users_id_login = []
    for i in range(len(users)):
        d = {}
        for x, y in users[i].items():
            if x == "id":
                d[x] = y
            if x == "login":
                d[x] = y
                users_id_login.append(d)

    return users_id_login


get_users_tasks()



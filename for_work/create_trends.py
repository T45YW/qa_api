import requests
import json

from lib.base_authorization import url_base, access_token_user, parent_id

# Входные данные
url = url_base + "/api/trend"
qt_trends = 10  # количество трендов -1
name_trend = "Trend "
parameter_uuid = "172473f2-76aa-47f6-9ea0-2149c30aed40"  # uuid параметра
trend_type = 2  # 1-interval, 2-change
max_record = 100  # максимальная запись
interval_ms = 1000
is_active = True
delta = 0.1

d_trends = {}  # словарь трендов {name: uuid}


def create_trends(name):
    """Создание трендов"""
    global d_trends
    def create_trend():
        """Создание тренда"""

        payload = json.dumps({
            "name": name,
            "parentId": parent_id,
            "parameterUuid": parameter_uuid,
            "trendTypeId": trend_type,
            "intervalMs": interval_ms,
            "maxRecord": max_record,
            "delta": delta,
            "isActive": is_active
        })
        headers = {
            'Authorization': access_token_user,
            'Content-Type': 'application/json'
        }

        response = requests.request("POST", url, headers=headers, data=payload)
        uuid_trend = response.json()["refUuid"]
        return uuid_trend

    for i in range(1, qt_trends):
        name = name_trend + str(i)
        d_trends.setdefault(name, create_trend())
    print(d_trends)
    return d_trends


create_trends(name_trend)

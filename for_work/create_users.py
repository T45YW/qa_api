import json
import requests

from lib.base_authorization import url_base, access_token_user


# Входные данные
login_user = "user"  # логин пользователя
full_name_user = "Пользователь "  # ФИО пользователя
qt_users = 2  # количество пользователей
state_id = 1  # 1-активный, 2-заблокированный
password = "123"

d_users = {}  # словарь пользователей {login: full name}


def create_users(login, full_name):
    """Создание пользователей"""
    global d_users

    def create_user():
        """Создание пользователя"""
        url = url_base + "/api/user"
        payload = json.dumps({
            "stateId": state_id,
            "login": login,
            "password": password,
            "fullName": full_name
        })
        headers = {
            'Authorization': access_token_user,
            'Content-Type': 'application/json'
        }
        response = requests.request("POST", url, headers=headers, data=payload)
        user_id = response.json()["id"]
        return user_id

    for i in range(1, qt_users+1):
        login = login_user + str(i)
        full_name = full_name_user + str(i)
        d_users.setdefault(login, create_user())

    print(d_users)
    return d_users


create_users(login_user, full_name_user)

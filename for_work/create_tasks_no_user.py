import json
import requests

from lib.base_authorization import url_base, access_token_user

# Входные данные
qt_tasks = 1  # количество задач
name_task = "Task"
priority = None  # 0≤x≤50:Низкий, 51≤х≤75:Средний, 76≤х≤100:Высокий
user_id_response = None  # Ответственный
date_done = 1720006611000000000  # срок выполнения, www.epochconverter.com. Поле обязательно, если есть отвественный

d_tasks = {}  # словарь задач {name: uuid}


def create_tasks():
    """Создание задач без ответственного"""
    global d_tasks

    def create_task(name):
        """Создание задачи без ответственного"""
        url = url_base + "/api/task"
        payload = json.dumps({
            "name": name,
            "priority": priority,
            "userIdResponse": user_id_response,
            "dateDone": date_done
        })
        headers = {
            'Authorization': access_token_user,
            'Content-Type': 'application/json'
        }

        response = requests.request("POST", url, headers=headers, data=payload)
        uuid_task = response.json()["uuid"]
        return uuid_task

    for i in range(1, qt_tasks + 1):
        name = name_task + str(i)
        d_tasks.setdefault(name, create_task(name))

    return d_tasks


create_tasks()
print(d_tasks)

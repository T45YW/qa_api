import json
import requests

from lib.base_authorization import url_base, access_token_user


# Входные данные
name_role = "Role "  # наименование роли
qt_roles = 3  # количество ролей

d_roles = {}  # словарь ролей {login: full name}



def create_roles(name):
    """Создание ролей"""
    def create_role():
        """Создание роли"""

        url = url_base + "/api/role"
        payload = json.dumps({
            "stateId": 1,
            "name": name,
        })
        headers = {
            'Authorization': access_token_user,
            'Content-Type': 'application/json'
        }
        response = requests.request("POST", url, headers=headers, data=payload)
        parsed_response_text = response.json()
        role_id = parsed_response_text["id"]
        return role_id

    for i in range(1, qt_roles+1):
        name = name_role + str(i)
        d_roles.setdefault(name, create_role())
    print(d_roles)
    return d_roles

create_roles(name_role)

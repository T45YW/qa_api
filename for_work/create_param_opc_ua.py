import json
import requests
from lib.base_authorization import access_token_user, parent_id, url_base
from create_controllers import uuid_opc_ua


# Входные данные физического параметра
name = "Параметр физический"
address_physic_param = "ns=1;s=Node1.PD_SIMULATOR.IntConst"  # Адрес
type_id = 4  # тип данных см.param_data_type
unit_id = 1  # ед.измерения см.param_unit
display_name = None  # string отображаемое наименование
edit_value = True  # Доступ: Чтение (False) или Запись (True)
min_value = None  # int минимальное значение
max_value = None  # int максимальное значение
bindings = None  # для вирт.параметра
out_bindings = None  # исх.привязка

uuid_param = ""


# Создание параметра
def create_param():
    global uuid_param
    url = url_base + "/api/parameter"
    payload = json.dumps({
        "parentId": parent_id,
        "name": name,
        "address": "[{\"name\":\"Address\",\"value\":\"" + address_physic_param + "\"}]",
        "timeAgentId": 0,
        "displayName": display_name,
        "dataTypeId": type_id,
        "controllerUuid": uuid_opc_ua,
        "unitId": unit_id,
        "isEditValue": True,
        "minValue": min_value,
        "maxValue": max_value,
        "binding": bindings,
        "OutBindings": out_bindings

    })
    headers = {
        'Authorization': access_token_user,
        'Content-Type': 'application/json'
    }
    response = requests.request("POST", url, headers=headers, data=payload)
    parsed_response_text = response.json()
    uuid_param = parsed_response_text["refUuid"]
    print(f"Uuid параметра: {uuid_param}")
    return uuid_param


create_param()




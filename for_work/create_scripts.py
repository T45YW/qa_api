import json
import requests
from lib.base_authorization import access_token_user, parent_id, url_base

# Входные данные
qt_scripts = 4  # количество скриптов -1
url = url_base + "/api/script"
name_script = "Скрипт "
body = "<?xml version=\"1.0\"?>"  # пустое тело: "<?xml version=\"1.0\"?>"
desc = None

d_scripts = {}  # словарь скриптов {name: uuid}


def create_scripts(name):
    """Создание скриптов"""
    global d_scripts

    def create_script():
        """Создание скрипта"""
        payload = json.dumps({
            "name": name,
            "parentId": parent_id,
            "body": body,
            "desc": desc
        })
        headers = {
            'Authorization': access_token_user,
            'Content-Type': 'application/json'
        }

        response = requests.request("POST", url, headers=headers, data=payload)
        parsed_response_text = response.json()
        uuid_script = parsed_response_text["refUuid"]
        return uuid_script

    for i in range(1, qt_scripts):
        name = name_script + str(i)
        d_scripts.setdefault(name, create_script())
    print(d_scripts)
    return d_scripts


create_scripts(name_script)

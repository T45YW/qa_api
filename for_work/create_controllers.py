import requests
import json
from lib.base_authorization import url_base, access_token_user, parent_id


# Входные данные
url_controllers = url_base + "/api/controller"
# OPC UA
name_opc_ua = "OPC UA 1"
ip_opc_ua = "192.168.5.40"

driver_opc_ua = ""
uuid_opc_ua = ""
# Modbus TCP
name_modbus_tcp = "Modbus TCP 1"
ip_modbus = "192.168.5.40"
port_modbus = "502"
id_modbus = "1"

driver_modbus_tcp = ""
uuid_modbus_tcp = ""
# BACnet
name_bacnet = "BACnet 1"
ip_local_bacnet = "192.168.5.40"
ip_bacnet = "192.168.5.67"

driver_bacnet = ""
uuid_bacnet = ""



def lst_drivers():
    """Получение списка драйверов"""
    global driver_opc_ua, driver_modbus_tcp, driver_bacnet
    url = url_base + "/api/drivers"

    payload = {}
    headers = {}

    response = requests.request("GET", url, headers=headers, data=payload)
    parsed_response_text = response.json()

    for i in range(len(parsed_response_text)):
        if parsed_response_text[i]["name"] == "OPC UA":
            driver_opc_ua = parsed_response_text[i]["uuid"]
        elif parsed_response_text[i]["name"] == "BACnet":
            driver_bacnet = parsed_response_text[i]["uuid"]
        elif parsed_response_text[i]["name"] == "Modbus TCP":
            driver_modbus_tcp = parsed_response_text[i]["uuid"]
    print(f"OPC UA: {driver_opc_ua}, \nModbus TCP: {driver_modbus_tcp}, \nBacNet: {driver_bacnet}")
    return driver_opc_ua, driver_modbus_tcp, driver_bacnet


def create_opc_ua():
    """создание OPC UA"""
    global uuid_opc_ua
    url = url_controllers

    payload = json.dumps({
        "parentId": parent_id,
        "name": name_opc_ua,
        "driverUuid": driver_opc_ua,
        "data": "[{\"Name\":\"Url\",\"Value\":\"opc.tcp://" + ip_opc_ua + ":54000\"}]"
    })
    headers = {
        'Authorization': access_token_user,
        'Content-Type': 'application/json'
    }

    response = requests.request("POST", url, headers=headers, data=payload)
    parsed_response_text = response.json()
    uuid_opc_ua = parsed_response_text["refUuid"]
    print()
    print(f"Uuid OPC UA: {uuid_opc_ua}")
    return uuid_opc_ua


def create_modbus_tcp():
    """создание Modbus TCP"""
    global uuid_modbus_tcp
    url = url_controllers

    payload = json.dumps({
        "parentId": parent_id,
        "name": name_modbus_tcp,
        "driverUuid": driver_modbus_tcp,
        "data": "[{\"Name\":\"Ip\",\"Value\":\"" + ip_modbus + "\"},{\"Name\":\"Port\",\"Value\":" + port_modbus + "},{\"Name\":\"UnitId\",\"Value\":" + id_modbus + "},{\"Name\":\"PeriodMS\",\"Value\":1000},{\"Name\":\"ConnectionTimeoutMS\",\"Value\":10000}]"
    })
    headers = {
        'Authorization': access_token_user,
        'Content-Type': 'application/json'
    }

    response = requests.request("POST", url, headers=headers, data=payload)
    parsed_response_text = response.json()
    uuid_modbus_tcp = parsed_response_text["refUuid"]
    print(f"Uuid Modbus TCP: {uuid_modbus_tcp}")
    return uuid_modbus_tcp


def create_bacnet():
    """cоздание BACnet"""
    global uuid_bacnet
    url = url_controllers

    payload = json.dumps({
        "parentId": parent_id,
        "name": name_bacnet,
        "driverUuid": driver_bacnet,
        "data": "[{\"Name\":\"LocalIP\",\"Value\":\"" + ip_local_bacnet + "\"},{\"Name\":\"Port\",\"Value\":47808},{\"Name\":\"DeviceIP\",\"Value\":\"" + ip_bacnet + "\"}]"
    })
    headers = {
        'Authorization': access_token_user,
        'Content-Type': 'application/json'
    }

    response = requests.request("POST", url, headers=headers, data=payload)
    parsed_response_text = response.json()
    uuid_bacnet = parsed_response_text["refUuid"]
    print(f"Uuid BACnet: {uuid_bacnet}")
    return uuid_bacnet


lst_drivers()
create_opc_ua()
# create_modbus_tcp()
# create_bacnet()

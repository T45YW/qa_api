import json

import requests

from lib.configuration import url_base, project_name, password_admin, login_admin, password_user, login_user

access_token_admin = ""  # токен админа в системе
project_uuid = ""   # uuid проекта
access_token_user = ""  # токен пользователя в проекте
parent_id = 0   # id пользовательской папки


def server_sign_in(login_admin, password_admin):
    """Авторизация в системе"""
    global access_token_admin
    url = url_base + "/api/server-sign-in"
    payload = json.dumps({
        "login": "admin",
        "password": "admin"
    })
    headers = {
        'Content-Type': 'application/json'
    }

    response = requests.request("POST", url, headers=headers, data=payload)
    access_token_admin = response.json()["access_token"]
    print("Авторизация на сервере прошла успешно")
    return access_token_admin


def get_projects():
    """Получение проекта"""
    global project_uuid
    url = url_base + "/api/projects"

    payload = {}
    headers = {}

    response = requests.request("GET", url, headers=headers, data=payload)
    parsed_response_text = response.json()

    for i in range(len(parsed_response_text)):
        if parsed_response_text[i]["name"] == project_name:
            project_uuid = parsed_response_text[i]["refUuid"]
    print(f"Получение проекта {project_name}")
    return project_uuid


def sign_in(login_user, password_user):
    """Авторизация в проекте"""
    global access_token_user

    url = url_base + "/api/sign-in"
    payload = json.dumps({
        "login": "admin",
        "password": "admin",
        "projectUuid": project_uuid
    })
    headers = {
        'Authorization': access_token_admin,
        'Content-Type': 'application/json'
    }

    response = requests.request("POST", url, headers=headers, data=payload)
    parsed_response_text = response.json()
    access_token_user = parsed_response_text["access_token"]
    print("Авторизация в проекте")
    return access_token_user


def project_tree():
    """Получение id пользовательской папки"""
    global parent_id
    url = url_base + "/api/project-tree/0"

    payload = {}
    headers = {
        'Authorization': access_token_user
    }

    response = requests.request("GET", url, headers=headers, data=payload)
    parsed_response_text = response.json()
    for i in range(len(parsed_response_text)):
        if parsed_response_text[i]["name"] == "Структура":
            parent_id = parsed_response_text[i]["id"]
    print("Id папки 'Структура' -", parent_id)
    return parent_id


server_sign_in(login_admin, password_admin)
get_projects()
sign_in(login_user, password_user)
project_tree()


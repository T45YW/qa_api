from lib.base_authorization import access_token_user, access_token_admin

headers_user = {
        'Authorization': access_token_user,
        'Content-Type': 'application/json'
    }

headers_admin = {
        'Authorization': access_token_admin,
        'Content-Type': 'application/json'
    }

headers_auth_project = {
  'Authorization': access_token_user
}

headers = {
    'Content-Type': 'application/json'}

headers_null = {
        'Authorization': 'null'
}

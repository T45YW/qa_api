import json
import requests

from lib.base_authorization import parent_id
from lib.configuration import url_base, OpcUa, ModbusTcp, BACnet, lst_drivers, User
from lib.general import headers_user

class CreateControllers:
    url = url_base + "/api/controller"

    def get_drivers(self):
        self.url = url_base + "/api/drivers"

        payload = {}
        headers = {}

        self.response = requests.request("GET", self.url, headers=headers, data=payload)
        self.result = self.response.json()

        self.dict_drivers = {}
        for i in range(len(self.result)):
            self.dict_drivers[self.result[i].get("name")] = self.result[i].get("uuid")

        if len(lst_drivers) == len(self.dict_drivers):
            return self.dict_drivers
        else:
            raise ValueError("Ошибка. Драйверы не получены")


    def create_opc_ua(self, url=url):
        payload = json.dumps({
            "parentId": parent_id,
            "name": OpcUa.name,
            "driverUuid": self.get_drivers().get(OpcUa.driver_name),
            "data": "[{\"Name\":\"Url\",\"Value\":\"" + OpcUa.url_opc_ua + "\"},"
                                                                           "{\"Name\":\"Login\",\"Value\":\"\"},"
                                                                           "{\"Name\":\"Password\",\"Value\":\"\"}]"
        })

        self.response = requests.request("POST", url, headers=headers_user, data=payload)
        self.opc_ua_uuid = self.response.json()["refUuid"]
        return self.opc_ua_uuid

    def create_modbus_tcp(self, url=url):
        payload = json.dumps({
            "parentId": parent_id,
            "name": ModbusTcp.name,
            "driverUuid": self.get_drivers().get(ModbusTcp.driver_name),
            "data": "[{\"Name\":\"Ip\",\"Value\":\"" + ModbusTcp.ip_modbus_tcp + "\"},{\"Name\":\"Port\",\"Value\":" + ModbusTcp.port + "},{\"Name\":\"UnitId\",\"Value\":" + ModbusTcp.id_device + "},{\"Name\":\"PeriodMS\",\"Value\":" + ModbusTcp.period_ms + "},{\"Name\":\"ConnectionTimeoutMS\",\"Value\":" + ModbusTcp.connection_timeout_ms + "},{\"Name\":\"MaxMultipleReadRegisters\",\"Value\":" + ModbusTcp.max_read_registers + "}]"
        })

        self.response = requests.request("POST", url, headers=headers_user, data=payload)
        self.modbus_tcp_uuid = self.response.json()["refUuid"]
        return self.modbus_tcp_uuid

    def create_bacnet(self, url=url):
        payload = json.dumps({
            "parentId": parent_id,
            "name": BACnet.name,
            "driverUuid": self.get_drivers().get(BACnet.driver_name),
            "data": "[{\"Name\":\"LocalIP\",\"Value\":\"" + BACnet.ip_bacnet + "\"},{\"Name\":\"Port\",\"Value\":" + BACnet.port + "},{\"Name\":\"DeviceIP\",\"Value\":\"" + BACnet.id_device + "\"}]"
        })

        self.result = requests.request("POST", url, headers=headers_user, data=payload)
        self.bacnet_uuid = self.result.json()["refUuid"]
        return self.bacnet_uuid


class CreateUser:
    def create_user(self):
        """Создание пользователя"""
        url = url_base + "/api/user"
        payload = json.dumps({
            "stateId": User.state_id,
            "login": User.login,
            "password": User.password,
            "fullName": User.full_name,
            "position": User.position,
            "phone": User.phone,
            "email": User.email,
            "desc": User.desc
        })

        response = requests.request("POST", url, headers=headers_user, data=payload)

        if response.status_code == 200:
            return response.json()["id"]
        else:
            raise ValueError("Ошибка. Пользователь не создан")

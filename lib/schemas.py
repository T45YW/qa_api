
AUTH_SCHEMA_JSON = {
    "type": "object",
    "properties": {
        "access_token": {"type": "string"},
        "refresh_token": {"type": "string"},
        "allowed_permissions": {
            "type": "array",
            "minItems": 5,
            "maxItems": 27,
            "items": {
                "type": "string",
            }
        }
    },
    "required": ["access_token", "refresh_token", "allowed_permissions"]
}




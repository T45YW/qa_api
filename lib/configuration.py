# ВХОДНЫЕ ДАННЫЕ
url_base = "http://192.168.5.40:8082"  # url
project_name = "main"  # наименование проекта
login_admin = "admin"  # логин пользователя в системе
password_admin = "admin"  # пароль пользователя в системе
login_user = "admin"  # логин пользователя в проекте
password_user = "admin"  # пароль пользователя в проекте
folder_name = "Структура"  # папка в которой будут создаваться объекты
qt_access_right = 27 # количество глобальных прав

# ДРАЙВЕРЫ
qt_drivers = 5  # количество драйверов в системе
lst_drivers = ["Modbus RTU", "OPC UA", "BACnet", "Modbus TCP", "Storage"]
class OpcUa:
    driver_name = "OPC UA"
    name = "Ист.данных OPC UA_1"
    url_opc_ua = "opc.tcp://192.168.5.68:54000"

class ModbusTcp:
    driver_name = "Modbus TCP"
    name = "Ист.данных Modbus_TCP_1"
    ip_modbus_tcp = "192.168.5.67"
    port = "503"
    id_device = "1"
    period_ms = "1000"
    connection_timeout_ms = "10000"
    max_read_registers = "10"

class BACnet:
    driver_name = "BACnet"
    name = "Ист.данных BACnet_2"
    ip_bacnet = "192.168.5.40"
    port = "47808"
    id_device = "192.168.5.67"


class User:
    state_id = 1  # или 2, 1-активный, 2-заблокированный
    login = "User1"
    password = "123pass!"
    full_name = "Ynit-Юнитович"
    position = "Директор"
    phone = "+7(927)402-30-53"
    email = "sd@root.ru"
    desc = "Проверка"


class BdData:
    host = 'localhost'
    user = 'postgres'
    password = 'postgres'
    port = '5432'  # по дефолту всегда используется 5432, можно не указывать
    db_name_ags = 'ags'
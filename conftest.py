import json
import pytest
import requests
import psycopg2

from lib.base_authorization import access_token_user
from lib.configuration import url_base, qt_drivers, lst_drivers, BdData
from lib.general import headers_user


def _del_user(result):
    if result is None:
        return f"url_del = {result}"
    else:
        url = url_base + result
        payload = {}
        headers = {
            'Authorization': access_token_user
        }
        response = requests.request("DELETE", url, headers=headers, data=payload)
        assert response.status_code == 200, {f"Удаление не прошло, {url}"}


@pytest.fixture
def del_obj():
    return _del_user


@pytest.fixture
def create_user():
    """Создание пользователя"""
    payload = json.dumps({
        "stateId": 1,
        "login": "Backend",
        "password": "123",
        "fullName": "Николай Фомин",
    })

    response = requests.request("POST", url_base + "/api/user", headers=headers_user, data=payload)
    if response.status_code == 200:
        user_id = response.json()["id"]
    else:
        user_id = None
    return user_id

@pytest.fixture
def create_role():
    """Создание роли"""
    payload = json.dumps({
        "stateId": 1,
        "name": "Role1",
        "desc": ""
    })
    response = requests.request("POST", url_base + "/api/role", headers=headers_user, data=payload)
    if response.status_code == 200:
        role_id = response.json()["id"]
    else:
        role_id = None

    return role_id


@pytest.fixture
def get_drivers():
    url = url_base + "/api/drivers"

    payload = {}
    headers = {}

    response = requests.request("GET", url, headers=headers, data=payload)
    result = response.json()

    dict_drivers = {}
    for i in range(len(result)):
        dict_drivers[result[i].get("name")] = result[i].get("uuid")

    if len(lst_drivers) == len(dict_drivers):
        return dict_drivers
    else:
        return None


@pytest.fixture
def connection_db_session():
    try:
        # пытаемся подключиться к базе данных
        connection = psycopg2.connect(
            host=BdData.host,
            user=BdData.user,
            password=BdData.password,
            dbname=BdData.db_name_ags
        )

        # тест
        yield connection.cursor()

    except:
        # в случае сбоя подключения будет выведено сообщение в STDOUT
        print('Ошибка. Не удалось подключиться к базе данных')

    finally:
        if connection:
            connection.close()

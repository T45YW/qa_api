# Qa API

Привет, строго не суди, это мой первый код и тебе придется все переписывать :)

## Запуск тестов

* py -m pytest -s -v -m smoke for_test - вызов маркированного теста < smoke >
* py -m pytest -s -v for_test  - запуск тестов из папки <for_test>
* py -m pytest -s -v for_test/test_get_projects.py - запуск одно теста <for_test/test_get_projects.py>

## Allure
* Установить allure [(тык с 5:10)](https://www.youtube.com/watch?v=6qASwPL86MM&ab_channel=RomanMatveev?t=5m10s)
* Генерация отчетов, пример: 
  * py -m pytest -s -v <for_test> --alluredir=report  - где report - наименование файла, куда сохранится отчет
  * allure serve report


## Справка

1. [обязательные входные данные](lib/configuration.py)
2. [авторизация в проектe](lib/base_authorization.py)
2. [фикстуры](conftest.py)
3. [headers](lib/general.py)
4. [ошибки](lib/base_response.py)
5. [json схемы](lib/schemas.py)
6. [запросы для тестов](lib/base_func.py)
7. [для быстрой работы](for_work) - здесь можно создать N пользователей, ролей, трендов и пр




import json

import pytest
import requests
from lib.base_authorization import url_base, project_uuid
from lib.general import headers_user, headers_admin
from lib.base_response import BaseResponse

login = "BackEnd"
password = "123"

states = [
    (1),
    (2)
]


@pytest.mark.parametrize('state', states)
def test_auth_custom_user(state, del_obj):
    """Авторизация тестового пользователя без прав в проекте"""

    # СОЗДАНИЕ ПОЛЬЗОВАТЕЛЯ
    payload = json.dumps({
        "stateId": state,
        "login": login,
        "password": password,
        "fullName": "Николай Фомин",
    })

    response1 = requests.request("POST", url_base+"/api/user", headers=headers_user, data=payload)

    if response1.status_code == 200:
        user_id = response1.json()["id"]
        url_del = "/api/user/" + str(user_id)
    else:
        url_del = None

    # АВТОРИЗАЦИЯ ПОЛЬЗОВАТЕЛЯ
    payload = json.dumps({
        "login": login,
        "password": password,
        "projectUuid": project_uuid
    })
    result = requests.request("POST", url_base+"/api/sign-in", headers=headers_admin, data=payload)
    response2 = BaseResponse(result)

    if state == 1:
        response2.assert_status_code(200)
    elif state == 2:
        response2.assert_status_code(401)

    del_obj(url_del)
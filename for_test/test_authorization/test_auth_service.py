import requests
import json
import pytest

from lib.base_authorization import url_base
from lib.general import headers
from lib.schemas import AUTH_SCHEMA_JSON
from lib.base_response import BaseResponse

# Входные данные
logins = [
    ("admin"),
    ("user"),
    ("")
]
passwords = [
    ("admin"),
    ("user")
]

@pytest.mark.smoke
@pytest.mark.parametrize('login', logins)
@pytest.mark.parametrize('password', passwords)
def test_server_sign_in(login, password):
    """Авторизация в проекте"""

    url = url_base + "/api/server-sign-in"
    payload: str = json.dumps({
        'login': login,
        'password': password
    })

    result = requests.request("POST", url, headers=headers, data=payload)
    response = BaseResponse(result)
    if login == password:
        response.assert_status_code(200).validate(AUTH_SCHEMA_JSON)
    elif login == "":
        response.assert_status_code(400)
    else:
        response.assert_status_code(401)

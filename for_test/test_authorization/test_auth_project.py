import requests
import json
import pytest

from lib.base_authorization import url_base, project_uuid
from lib.general import headers_admin
from lib.schemas import AUTH_SCHEMA_JSON
from lib.base_response import BaseResponse


logins = [
    ("admin"),
    ("user"),
    ("")
]
passwords = [
    ("admin"),
    ("user")
]
@pytest.mark.smoke
@pytest.mark.parametrize('login', logins)
@pytest.mark.parametrize('password', passwords)
def test_sign_in(login, password):
    """Авторизация на сервере"""
    url = url_base + "/api/sign-in"

    payload = json.dumps({
        "login": login,
        "password": password,
        "projectUuid": project_uuid
    })

    result = requests.request("POST", url, headers=headers_admin, data=payload)
    response = BaseResponse(result)
    if login == "admin" and password == "admin":
        response.assert_status_code(200).validate(AUTH_SCHEMA_JSON)
    elif login == "":
        response.assert_status_code(400)
    else:
        response.assert_status_code(401)


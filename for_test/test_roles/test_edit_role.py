import requests
import json
import pytest
from lib.base_authorization import url_base
from lib.general import headers_user
from lib.base_response import BaseResponse


names = [
    ("Poль!"),
    ("")
]
descs = [
    ("АвтоRole1!")
]

@pytest.mark.parametrize('name', names)
@pytest.mark.parametrize('desc', descs)
def test_edit_role(create_role, name, desc, del_obj):
    """Редактирование роли"""
    # СОЗДАНИЕ РОЛИ
    role_id = create_role
    if role_id is None:
        return "Ошибка. Роль не создана"
    else:
        url_del = "/api/role/" + str(role_id)

    # РЕДАКТИРОВАНИЕ
    url = url_base+"/api/role"

    payload = json.dumps({
      "id": role_id,
      "stateId": 2,
      "name": name,
      "desc": desc
    })

    response = requests.request("PUT", url, headers=headers_user, data=payload)
    result = BaseResponse(response)

    if len(name) == 0:
        result.assert_status_code(400)
    else:
        result.assert_status_code(200)

    del_obj(url_del)
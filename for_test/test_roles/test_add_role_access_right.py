import requests
import json
import pytest
from lib.base_authorization import url_base
from lib.general import headers_user
from lib.base_response import BaseResponse

@pytest.mark.smoke
def test_add_role_access_right_1(create_role, del_obj):
    """Добавление роли глобальных прав администратора системы"""
    # СОЗДАНИЕ РОЛИ
    role_id = create_role
    if role_id is None:
        return print("Ошибка. Роль не создана")

    # ПРИВЯЗКА ПРАВА ДОСТУПА К РОЛИ
    url = url_base+"/api/role-access-right"

    payload = json.dumps({
        "roleId": role_id,
        "accessRightIds": [1]
    })

    response = requests.request("POST", url, headers=headers_user, data=payload)
    result = BaseResponse(response)
    result.assert_status_code(200)
    assert "ok" in response.text, print(f"Ожидаемый ответ: 'result':'ok', фактический результат {response.text}")

    url_del = "/api/role/" + str(role_id)
    del_obj(url_del)


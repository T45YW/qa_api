import pytest
import requests

from lib.base_authorization import url_base
from lib.general import headers_user
from lib.base_response import BaseResponse

@pytest.mark.smoke
def test_get_root():
    """Получение всех ролей в проекте
    Проверка на пользователя по умолчанию с ролью root"""
    url = url_base+"/api/roles"

    payload = {}
    response = requests.request("GET", url, headers=headers_user, data=payload)
    result = BaseResponse(response)

    result.assert_status_code(200)

    if response.status_code == 200:
        assert "root" in response.text, "Роли 'root' нет  проекте"

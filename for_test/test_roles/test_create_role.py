import requests
import json
import pytest
from lib.base_authorization import url_base
from lib.general import headers_user
from lib.base_response import BaseResponse


states = [
    (1),
    (2)
]
names = [
    ("Роль1"),
    ("")
]
descs = [
    ("Автороль"),
    ("")
]
@pytest.mark.smoke
@pytest.mark.parametrize('state', states)
@pytest.mark.parametrize('name', names)
@pytest.mark.parametrize('desc', descs)
def test_create_role(state, name, desc, del_obj):
    """Cоздание роли"""
    url = url_base+"/api/role"

    payload = json.dumps({
      "stateId": state,
      "name": name,
      "desc": desc
    })

    response = requests.request("POST", url, headers=headers_user, data=payload)
    result = BaseResponse(response)

    if len(name) == 0:
        result.assert_status_code(400)
        url_del = None
    else:
        result.assert_status_code(200)
        url_del = "/api/role/" + str(response.json()["id"])

    del_obj(url_del)

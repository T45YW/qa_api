import pytest
import requests

from lib.base_authorization import url_base
from lib.general import headers_user
from lib.configuration import qt_access_right
from lib.base_response import BaseResponse

@pytest.mark.smoke
def test_get_access_right():
    """Получение всех глобальных прав в проекте"""
    url = url_base + "/api/access-right"
    payload = {}
    response = requests.request("GET", url, headers=headers_user, data=payload)
    result = BaseResponse(response)

    result.assert_status_code(200)
    if response.status_code == 200:
        x = len(response.json())
        assert x >= qt_access_right, f"Количество глобальных прав:{x},должно быть: {qt_access_right}"










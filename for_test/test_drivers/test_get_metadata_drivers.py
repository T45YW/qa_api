import pytest
import requests
from lib.configuration import url_base
from lib.base_func import CreateControllers
from lib.base_response import BaseResponse
from lib.configuration import lst_drivers

a = CreateControllers()
drivers = [
    (a.get_drivers().get(lst_drivers[i])) for i in range(len(lst_drivers))
]

@pytest.mark.parametrize('driver', drivers)
def test_get_metadata_drivers(driver):
    url = url_base + "/api/metadata/" + driver

    payload = {}
    headers = {}

    result = requests.request("GET", url, headers=headers, data=payload)
    response = BaseResponse(result)

    response.assert_status_code(200)
    assert (result.json()["protocolName"]) in lst_drivers, "ОШИБКА. Драйвер не найден"


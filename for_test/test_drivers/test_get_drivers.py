import requests
from lib.configuration import url_base, qt_drivers, lst_drivers
from lib.base_response import BaseResponse

def test_get_drivers():
    url = url_base + "/api/drivers"

    payload = {}
    headers = {}

    response = requests.request("GET", url, headers=headers, data=payload)
    result = BaseResponse(response)

    result.assert_status_code(200)
    assert len(response.json()) == qt_drivers, f"Ожидаемое число драйверов: {qt_drivers}, фактическое: {len(result)}"

    name_drivers = [response.json()[i].get("name") for i in range(len(response.json()))]

    for i in range(len(lst_drivers)):
        assert lst_drivers[i] in name_drivers, f"{lst_drivers[i]} нет в {name_drivers}"

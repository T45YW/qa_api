import json

import pytest
import requests
from lib.base_authorization import url_base
from lib.general import headers_user
from lib.base_response import BaseResponse
from lib.base_func import CreateUser

states = [
    (1),
    (2)
]
logins = [
    ("юзер"),
    ("")
]
@pytest.mark.parametrize('state', states)
@pytest.mark.parametrize('login', logins)
def test_edit_user(create_user, login, state, del_obj):
    """Редактирование пользователя"""

    # СОЗДАНИЕ ПОЛЬЗОВАТЕЛЯ
    a = CreateUser()
    user_id = a.create_user()
    url_del = "/api/user/" + str(user_id)

    # РЕДАКТИРОВАНИЕ
    url = url_base + "/api/user"
    payload = json.dumps({
        "stateId": state,
        "id": user_id,
        "login": login,
        # "password": password,  #  пользователь не может изменить пароль
        "fullName": None,
        "position": None,
        "phone": None,
        "email": None,
        "desc": "Наследие Коляша"
    })

    response = requests.request("PUT", url, headers=headers_user, data=payload)
    result = BaseResponse(response)

    if len(login) == 0:
        result.assert_status_code(400)
    else:
        result.assert_status_code(200)
        assert "ok" in response.text, f"В теле ответа нет атрибута 'result:ok' - {response.text}"

    del_obj(url_del)












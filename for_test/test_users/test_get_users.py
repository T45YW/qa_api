import requests
import pytest
from lib.base_authorization import url_base, access_token_user
from lib.base_response import BaseResponse
from lib.general import headers_user


user_name = "admin"
full_name = "admin"

@pytest.mark.smoke
def test_get_projects():
    """Получение списка пользователей"""
    url = url_base + "/api/users"
    payload = {}

    response = requests.request("GET", url, headers=headers_user, data=payload)
    result = BaseResponse(response)
    result.assert_status_code(200)

    if response.status_code == 200:
        for i in range(len(response.json())):
            assert response.json()[i]["login"] == user_name and response.json()[i]["fullName"] == full_name, \
                f"'{user_name}' no in response"
            break


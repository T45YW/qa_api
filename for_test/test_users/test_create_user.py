import requests
import json
import pytest
from lib.base_authorization import url_base
from lib.general import headers_user
from lib.base_response import BaseResponse
from lib.configuration import User

states = [
    (1),
    (2)
]
logins = [
    (User.login),
    ("")
]
passwords = [
    (User.password),
    ("")
]
full_names = [
    (User.full_name),
    ("")
]
@pytest.mark.smoke
@pytest.mark.parametrize('state', states)
@pytest.mark.parametrize('login', logins)
@pytest.mark.parametrize('password', passwords)
@pytest.mark.parametrize('full_name', full_names)
def test_create_user(state, login, password, full_name, del_obj):
    """Тест на создание пользователя"""
    url = url_base + "/api/user"
    payload = json.dumps({
        "stateId": state,
        "login": login,
        "password": password,
        "fullName": full_name,
        "position": User.position,
        "phone": User.phone,
        "email": User.email,
        "desc": User.desc
    })

    response = requests.request("POST", url, headers=headers_user, data=payload)
    result = BaseResponse(response)

    if len(login) > 0 and len(password) > 0 and len(full_name) > 0:
        url_del = "/api/user/" + str(response.json()["id"])
        result.assert_status_code(200)
    elif len(login) == 0 or len(password) == 0 or len(full_name) == 0:
        url_del = None
        result.assert_status_code(400)

    del_obj(url_del)

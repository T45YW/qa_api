from lib.base_func import CreateUser


def test_get_db_user(connection_db_session, del_obj):
    a = CreateUser()
    user_uuid = a.create_user()
    url_del = "/api/user/" + str(user_uuid)

    with connection_db_session as cursor:
        cursor.execute(
            f"SELECT * FROM admin.user WHERE id={user_uuid}"
        )
        b = cursor.fetchone()

    assert user_uuid in b, f"Пользователь с id={user_uuid} не найден в базе данных"
    del_obj(url_del)

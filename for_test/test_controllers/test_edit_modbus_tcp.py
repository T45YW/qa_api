import pytest
import requests
import json

from lib.base_func import CreateControllers
from lib.base_authorization import parent_id
from lib.general import headers_user
from lib.configuration import url_base, ModbusTcp
from lib.base_response import BaseResponse


names_controller = [
    ("Modbus TCP 1!")
]
ips = [
    ("192.168.5.40")
]
ports = [
    ("1")
]
id_devices = [
    ("2")
]

periods_ms = [
    ("27")
]
timeouts_ms = [
    ("500")
]
read_registers = [
    ("125")
]

@pytest.mark.smoke
@pytest.mark.parametrize("name_ctr", names_controller)
@pytest.mark.parametrize("ip", ips)
@pytest.mark.parametrize("port", ports)
@pytest.mark.parametrize("id_device", id_devices)
@pytest.mark.parametrize("period_ms", periods_ms)
@pytest.mark.parametrize("timeout_ms", timeouts_ms)
@pytest.mark.parametrize("read_register", read_registers)
def test_create_modbus_tcp(get_drivers, name_ctr, ip, port, id_device, period_ms, timeout_ms, read_register, del_obj):
    """Редактирование источника данных Modbus TCP"""

    url = url_base + "/api/controller"

    cl = CreateControllers()
    modbus_tcp_uuid = cl.create_modbus_tcp()

    payload = json.dumps({
      "uuid": modbus_tcp_uuid,
      "name": name_ctr,
      "data": "[{\"Name\":\"Ip\",\"Value\":\""+ip+"\"},"
                                                  "{\"Name\":\"Port\",\"Value\":"+port+"},"
                                                  "{\"Name\":\"UnitId\",\"Value\":"+id_device+"},"
                                                  "{\"Name\":\"PeriodMS\",\"Value\":"+period_ms+"},"
                                                  "{\"Name\":\"ConnectionTimeoutMS\",\"Value\":"+timeout_ms+"},"
                                                  "{\"Name\":\"MaxMultipleReadRegisters\",\"Value\":"+read_register+"}]"
    })

    result = requests.request("PUT", url, headers=headers_user, data=payload)
    response = BaseResponse(result)

    if len(name_ctr) > 1 and len(ip) > 1 and len(port) > 1 and len(id_device) > 0\
            and len(period_ms) > 1 and len(timeout_ms) > 1 and len(read_register) > 0:
        response.assert_status_code(200)

    del_obj("/api/controller/" + modbus_tcp_uuid)

import pytest
import requests
import json

from lib.base_authorization import parent_id
from lib.general import headers_user
from lib.configuration import url_base, BACnet
from lib.base_response import BaseResponse

names_controller = [
    (BACnet.name),
    ("")
]
ips = [
    (BACnet.ip_bacnet),
    ("")
]
ports = [
    (BACnet.port),
    ("")
]

id_devices = [
    (BACnet.id_device)
]

@pytest.mark.smoke
@pytest.mark.parametrize("name_ctr", names_controller)
@pytest.mark.parametrize("ip", ips)
@pytest.mark.parametrize("port", ports)
@pytest.mark.parametrize("id_device", id_devices)
def test_create_bacnet(get_drivers, name_ctr, ip, port, id_device, del_obj):
    """Создание источника данных BACnet"""
    if get_drivers is not None:
        driver_uuid = get_drivers.get("BACnet")
    else:
        driver_uuid = None

    url = url_base+"/api/controller"

    payload = json.dumps({
      "parentId": parent_id,
      "name": name_ctr,
      "driverUuid": driver_uuid,
      "data": "[{\"Name\":\"LocalIP\",\"Value\":\""+ip+"\"},{\"Name\":\"Port\",\"Value\":"+port+"},{\"Name\":\"DeviceIP\",\"Value\":\""+id_device+"\"}]"
    })

    result = requests.request("POST", url, headers=headers_user, data=payload)
    response = BaseResponse(result)

    if len(name_ctr) > 2 and len(ip) > 1 and len(port) > 1 and len(id_device) > 1:
        response.assert_status_code(200)
        assert result.json()[
                   "name"] == name_ctr, f"Ошибка. Наименованием ист.данных '{name_ctr}'!={result.json()['name']}"
        del_obj("/api/controller/" + result.json()["refUuid"])
    else:
        response.assert_status_code(400)
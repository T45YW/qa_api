import pytest
import requests
import json

from lib.base_authorization import parent_id
from lib.general import headers_user
from lib.configuration import url_base, BACnet
from lib.base_response import BaseResponse
from lib.base_func import CreateControllers

names_controller = [
    ("Name_Имя !")
]
ips = [
    ("192.168.5.40")
]
ports = [
    ("159")
]

id_devices = [
    ("192.168.5.67")
]

@pytest.mark.smoke
@pytest.mark.parametrize("name_ctr", names_controller)
@pytest.mark.parametrize("ip", ips)
@pytest.mark.parametrize("port", ports)
@pytest.mark.parametrize("id_device", id_devices)
def test_create_bacnet(name_ctr, ip, port, id_device, del_obj):
    """Редактирование источника данных BACnet"""
    url = url_base+"/api/controller"

    cl = CreateControllers()
    bacnet_uuid = cl.create_bacnet()

    payload = json.dumps({
      "uuid": bacnet_uuid,
      "name": name_ctr,
      "data": "[{\"Name\":\"LocalIP\",\"Value\":\""+ip+"\"},{\"Name\":\"Port\",\"Value\":"+port+"},{\"Name\":\"DeviceIP\",\"Value\":\""+id_device+"\"}]"
    })

    result = requests.request("PUT", url, headers=headers_user, data=payload)
    response = BaseResponse(result)

    if len(name_ctr) > 2 and len(ip) > 1 and len(port) > 1 and len(id_device) > 1:
        response.assert_status_code(200)

    del_obj("/api/controller/" + bacnet_uuid)

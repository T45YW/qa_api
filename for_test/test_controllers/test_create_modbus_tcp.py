import pytest
import requests
import json

from lib.base_authorization import parent_id
from lib.general import headers_user
from lib.configuration import url_base, ModbusTcp
from lib.base_response import BaseResponse


names_controller = [
    (ModbusTcp.name),
    ("")
]
ips = [
    (ModbusTcp.ip_modbus_tcp),
    ("")
]
ports = [
    (ModbusTcp.port),
    ("")
]
id_devices = [
    (ModbusTcp.id_device),
    ("")
]

periods_ms = [
    (ModbusTcp.connection_timeout_ms),
    ("")
]
timeouts_ms = [
    (ModbusTcp.period_ms),
    ("")
]
read_registers = [
    (ModbusTcp.max_read_registers),
    ("")
]

@pytest.mark.smoke
@pytest.mark.parametrize("name_ctr", names_controller)
@pytest.mark.parametrize("ip", ips)
@pytest.mark.parametrize("port", ports)
@pytest.mark.parametrize("id_device", id_devices)
@pytest.mark.parametrize("period_ms", periods_ms)
@pytest.mark.parametrize("timeout_ms", timeouts_ms)
@pytest.mark.parametrize("read_register", read_registers)
def test_create_modbus_tcp(get_drivers, name_ctr, ip, port, id_device, period_ms, timeout_ms, read_register, del_obj):
    """Создание источника данных Modbus TCP"""
    if get_drivers is not None:
        driver_uuid = get_drivers.get("Modbus TCP")
    else:
        driver_uuid = None

    url = url_base + "/api/controller"

    payload = json.dumps({
      "parentId": parent_id,
      "name": name_ctr,
      "driverUuid": driver_uuid,
      "data": "[{\"Name\":\"Ip\",\"Value\":\""+ip+"\"},"
                                                  "{\"Name\":\"Port\",\"Value\":"+port+"},"
                                                  "{\"Name\":\"UnitId\",\"Value\":"+id_device+"},"
                                                  "{\"Name\":\"PeriodMS\",\"Value\":"+period_ms+"},"
                                                  "{\"Name\":\"ConnectionTimeoutMS\",\"Value\":"+timeout_ms+"},"
                                                 "{\"Name\":\"MaxMultipleReadRegisters\",\"Value\":"+read_register+"}]"
    })

    result = requests.request("POST", url, headers=headers_user, data=payload)
    response = BaseResponse(result)

    if len(name_ctr) > 1 and len(ip) > 1 and len(port) > 1 and len(id_device) > 0\
            and len(period_ms) > 1 and len(timeout_ms) > 1 and len(read_register) > 0:
        response.assert_status_code(200)
        assert result.json()["name"] == name_ctr, f"Ошибка. Наименованием ист.данных '{name_ctr}'!={result.json()['name']}"
        del_obj("/api/controller/" + result.json()["refUuid"])
    else:
        response.assert_status_code(400)


import pytest
import requests
import json
from lib.base_func import CreateControllers
from lib.base_response import BaseResponse
from lib.configuration import url_base
from lib.general import headers_user


class TestGetControllers:
    """Получение контроллеров"""
    cl = CreateControllers()
    payload = {}

    def test_get_opc_ua(self, del_obj):
        """Получение источника данных OPC UA"""
        opc_ua_uuid = self.cl.create_opc_ua()
        url = url_base + "/api/controller/" + opc_ua_uuid

        result = requests.request("GET", url, headers=headers_user, data=self.payload)
        response = BaseResponse(result)
        if response.assert_status_code(200):
            assert opc_ua_uuid == result.json()["refUuid"], f"Ожидаемый 'refUuid': {opc_ua_uuid}, фактический {result.json()['refUuid']}"

        del_obj("/api/controller/" + opc_ua_uuid)


    def test_get_modbus_tcp(self, del_obj):
        """Получение источника данных Modbus TCP"""
        modbus_tcp_uuid = self.cl.create_modbus_tcp()
        url = url_base + "/api/controller/" + modbus_tcp_uuid

        result = requests.request("GET", url, headers=headers_user, data=self.payload)
        response = BaseResponse(result)
        if response.assert_status_code(200):
            assert modbus_tcp_uuid == result.json()["refUuid"], f"Ожидаемый 'refUuid': {modbus_tcp_uuid}, фактический {result.json()['refUuid']}"
        del_obj("/api/controller/" + modbus_tcp_uuid)


    def test_get_bacnet(self, del_obj):
        """Получение источника данных BACnet"""
        bacnet_uuid = self.cl.create_bacnet()
        url = url_base + "/api/controller/" + bacnet_uuid

        result = requests.request("GET", url, headers=headers_user, data=self.payload)
        response = BaseResponse(result)
        if response.assert_status_code(200):
            assert bacnet_uuid == result.json()["refUuid"], f"Ожидаемый 'refUuid': {bacnet_uuid}, фактический {result.json()['refUuid']}"
        del_obj("/api/controller/" + bacnet_uuid)

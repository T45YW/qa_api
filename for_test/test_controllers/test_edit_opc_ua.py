import pytest
import requests
import json
from lib.base_func import CreateControllers
from lib.base_response import BaseResponse
from lib.configuration import url_base, OpcUa
from lib.general import headers_user



names_controller = [
    (OpcUa.name)
]
url = [
    (OpcUa.url_opc_ua)
]
logins = [
    ("loгин1!@"),
    ("")
]
passwords = [
    ("passворд1!@"),
    ("")
]

@pytest.mark.parametrize("name_ctr", names_controller)
@pytest.mark.parametrize("name_url", url)
@pytest.mark.parametrize("login", logins)
@pytest.mark.parametrize("password", passwords)
def test_edit_opc_ua(name_ctr, name_url, login, password, del_obj):
    """Редактирование источника данных OPC UA"""
    url = url_base + "/api/controller"

    cl = CreateControllers()
    opc_ua_uuid = cl.create_opc_ua()

    payload = json.dumps({
        "uuid": opc_ua_uuid,
        "name": name_ctr,
        "data": "[{\"Name\":\"Url\",\"Value\":\""+name_url+"\"},{\"Name\":\"Login\",\"Value\":\""+login+"\"},"
              "{\"Name\":\"Password\",\"Value\":\""+password+"\"}]"
    })

    result = requests.request("PUT", url, headers=headers_user, data=payload)
    response = BaseResponse(result)
    if len(name_ctr) > 1 and len(name_url) > 1:
        response.assert_status_code(200)

    del_obj("/api/controller/" + opc_ua_uuid)
















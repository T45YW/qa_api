import pytest
import requests
import json

from lib.base_authorization import parent_id
from lib.general import headers_user
from lib.configuration import url_base, OpcUa
from lib.base_response import BaseResponse


names_controller = [
    (OpcUa.name),
    ("")
]
url = [
    (OpcUa.url_opc_ua),
    ("")
]
logins = [
    ("loгин1!@"),
    ("")
]
passwords = [
    ("passворд1!@"),
    ("")
]

@pytest.mark.smoke
@pytest.mark.parametrize("name_ctr", names_controller)
@pytest.mark.parametrize("name_url", url)
@pytest.mark.parametrize("login", logins)
@pytest.mark.parametrize("password", passwords)
def test_create_OPC_UA(get_drivers, name_ctr, name_url, login, password, del_obj):
    """Создание источника данных OPC UA"""
    if get_drivers is not None:
        driver_uuid = get_drivers.get("OPC UA")
    else:
        driver_uuid = None

    url = url_base + "/api/controller"

    payload = json.dumps({
      "parentId": parent_id,
      "name": name_ctr,
      "driverUuid": driver_uuid,
      "data": "[{\"Name\":\"Url\",\"Value\":\""+name_url+"\"},{\"Name\":\"Login\",\"Value\":\""+login+"\"},"
              "{\"Name\":\"Password\",\"Value\":\""+password+"\"}]"
    })

    result = requests.request("POST", url, headers=headers_user, data=payload)
    response = BaseResponse(result)

    if len(name_ctr) > 0 and len(name_url) > 0:
        response.assert_status_code(200)
        assert result.json()["name"] == name_ctr, f"Ошибка. Наименованием ист.данных '{name_ctr}'!={result.json()['name']}"
        del_obj("/api/controller/" + result.json()["refUuid"])
    elif len(name_ctr) or len(name_url) == 0:
        response.assert_status_code(400)


import requests
from lib.base_authorization import url_base, project_name
from lib.general import headers_null
from lib.base_response import BaseResponse


def test_get_projects():
    """Получение всех проектов в системе"""
    url = url_base + "/api/projects"
    payload = {}

    response = requests.request("GET", url, headers=headers_null, data=payload)
    result = BaseResponse(response)
    result.assert_status_code(200)

    name_in_project = []
    if response.status_code == 200:
        for i in range(len(response.json())):
            name_in_project.append(response.json()[i]["name"])
    assert project_name in name_in_project, f"{project_name} no in projects"



